package com.jwong.common.web.interceptor;

import feign.RequestInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: Feign请求拦截器，将Token传递给下游服务
 * 如果hystrix开启的线程池策略，则必须保证hystrix.shareSecurityContext为true
 * 如果hystrix开启的信号量策略，可实现一个RequestInterceptor从RequestContextHolder中获取Token
 * 如果hystrix开启的线程池策略，并且希望从RequestContextHolder中获取Token，则需定制hystrix插件
 * @author:JWong
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(RequestInterceptor.class)
public class ClientOAuth2FeignRequestInterceptorConfiguration {

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "hystrix", name = "shareSecurityContext", matchIfMissing = true)
    public ClientOAuth2FeignRequestInterceptor clientOAuth2FeignRequestInterceptor() {
        return new ClientOAuth2FeignRequestInterceptor();
    }

}
