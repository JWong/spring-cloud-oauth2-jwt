package com.jwong.common.web.exception;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class ExceptionHandlerAdviceConfiguration {

    @Bean
    public DefaultExceptionHandlerAdvice defaultExceptionHandlerAdvice() {
        return new DefaultExceptionHandlerAdvice();
    }

}
