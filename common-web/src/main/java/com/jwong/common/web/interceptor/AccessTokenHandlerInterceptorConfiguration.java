package com.jwong.common.web.interceptor;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration(proxyBeanMethods = false)
public class AccessTokenHandlerInterceptorConfiguration {

    @Bean
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    @ConditionalOnProperty(prefix = "security.oauth2.resource.jwt", name = "keyValue")
    @ConditionalOnBean(value = {
            ResourceServerTokenServices.class, AccessTokenConverter.class
    })
    public AccessTokenHandlerInterceptor accessTokenHandlerInterceptor(
            ResourceServerTokenServices resourceServerTokenServices, AccessTokenConverter accessTokenConverter) {
        return new AccessTokenHandlerInterceptor(resourceServerTokenServices, accessTokenConverter);
    }

}
