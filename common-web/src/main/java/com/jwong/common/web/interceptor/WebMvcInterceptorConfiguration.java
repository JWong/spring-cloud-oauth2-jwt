package com.jwong.common.web.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Nonnull;
import java.util.Optional;

public class WebMvcInterceptorConfiguration implements WebMvcConfigurer {

    @Autowired
    private AccessTokenHandlerInterceptor accessTokenHandlerInterceptor;

    @Override
    public void addInterceptors(@Nonnull InterceptorRegistry registry) {
        Optional.of(accessTokenHandlerInterceptor).ifPresent(accessTokenHandlerInterceptor ->
            registry
                    .addInterceptor(accessTokenHandlerInterceptor)
                    .addPathPatterns("/**")
                    .excludePathPatterns("/v2/api-docs", "/swagger-resources/configuration/ui",
                            "/swagger-resources","/swagger-resources/configuration/security",
                            "/swagger-ui.html", "/webjars/**", "**/favicon.ico")
        );
    }

}