##

運行該示例前，請先啟動Redis、MYSQL，並將script目錄下的.sql文件應用於MYSQL

庫中預存了三個用戶:test1、test3、admin, 密碼均為123456

可使用三個用戶分別獲取token後，在token測試模塊使用Token

對於短信驗證碼登錄可以先模擬獲取一個驗證碼，然後使用手機號和該驗證碼獲取Token

![image](./images/01.PNG)

### 獲取Token

![image](./images/02.PNG)

### 使用Token

![image](./images/03.PNG)