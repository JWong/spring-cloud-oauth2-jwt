package com.jwong.common.entity;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class AccessToken implements Serializable {

    private final String accessToken;

    private final String tokenType;

    private final String refreshToken;

    private long expiresIn;

    private final Set<String> scope;

    private final Map<String, Object> additional;

    public AccessToken(String accessToken, String tokenType, String refreshToken,
                       long expiresIn, Set<String> scope, Map<String, Object> additional) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
        this.scope = scope;
        this.additional = additional;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public Set<String> getScope() {
        return scope;
    }

    public Map<String, Object> getAdditional() {
        return additional;
    }

    public static AccessTokenBuilder withAccessToken(String accessToken) {
        return builder().accessToken(accessToken);
    }

    public static AccessTokenBuilder builder() {
        return new AccessTokenBuilder();
    }

    public static class AccessTokenBuilder {

        private String accessToken;
        private String tokenType;
        private String refreshToken;
        private long expiresIn;
        private Set<String> scope;
        private Map<String, Object> additional;

        private AccessTokenBuilder() {
        }

        public AccessTokenBuilder accessToken(String accessToken) {
            this.accessToken = accessToken;
            return this;
        }

        public AccessTokenBuilder tokenType(String tokenType) {
            this.tokenType = tokenType;
            return this;
        }

        public AccessTokenBuilder refreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
            return this;
        }

        public AccessTokenBuilder expiresIn(long expiresIn) {
            this.expiresIn = expiresIn;
            return this;
        }

        public AccessTokenBuilder scope(Set<String> scope) {
            this.scope = scope;
            return this;
        }

        public AccessTokenBuilder additional(Map<String, Object> additional) {
            this.additional = additional;
            return this;
        }

        public AccessToken build() {
            return new AccessToken(accessToken, tokenType, refreshToken, expiresIn, scope, additional);
        }
    }


}
