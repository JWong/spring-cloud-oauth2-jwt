package com.jwong.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

public class SmsCode {

    public SmsCode() {}

    public SmsCode(String code, long expire) {
        this.code = code;
        this.expire = expire;
    }

    public SmsCode(String code, long expire, String mobile) {
        this(code, expire);
        this.mobile = mobile;
    }

    /* 验证码 */
    private String code;

    /* 手机号 */
    private String mobile;

    /* 过期时间, 毫秒值 */
    private long expire;

    /* 新建验证码的时间 */
    private final long createTime = new Date().getTime();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }

    public long getCreateTime() {
        return createTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /* 注意：Json会将符合Bean规范的方法返回值结果一并进行序列化 */
    @JsonIgnore
    public boolean isExpired() {
        return new Date().getTime() - this.createTime >= this.expire;
    }
}