package com.jwong.common.entity;

public class FeignResponseErrorBody {

    public FeignResponseErrorBody(){}

    // {"error":"invalid_grant","error_description":"验证码已过期，请重新获取"}

    private String error;

    private String errorDescription;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
