package com.jwong.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "发送短信验证码时的响应信息")
public class SmsResponse {

    /**
     * 是否成功
     */
    @ApiModelProperty(value = "表示短信发送是否成功")
    private boolean success;

    /**
     * 状态码
     */
    @ApiModelProperty(value = "返回的状态码")
    private Integer code;

    /**
     * 返回消息
     */
    @ApiModelProperty(value = "返回的消息")
    private String message;

    //
    private String smsCode;

}
