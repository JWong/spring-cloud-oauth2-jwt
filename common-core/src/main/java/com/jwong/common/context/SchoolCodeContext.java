package com.jwong.common.context;

import com.jwong.common.util.InstanceUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SchoolCodeContext {

    public static final String SCHOOL_CODE_KEY = "school_code_key";

    private SchoolCodeContext() {}

    private static CommonContext<String, String> CONTEXT_HOLDER =
            InstanceUtils.instance(ThreadLocalCommonContext.class, SchoolCodeContext.class.getClassLoader());

    public static String get() {
        String schoolCode = CONTEXT_HOLDER.get(SCHOOL_CODE_KEY);
        if (log.isDebugEnabled()) {
            log.debug("get schoolCodeKey from SchoolCodeContext: {}", schoolCode);
        }
        return schoolCode;
    }

    public static void bind(String schoolCode) {
        if (log.isDebugEnabled()) {
            log.debug("bind schoolCodeKey to SchoolCodeContext: {}", schoolCode);
        }
        CONTEXT_HOLDER.put(SCHOOL_CODE_KEY, schoolCode);
    }

    public static String unbind() {
        String schoolCode = CONTEXT_HOLDER.remove(SCHOOL_CODE_KEY);
        if (log.isDebugEnabled()) {
            log.debug("unbind schoolCodeKey from SchoolCodeContext: {} ", schoolCode);
        }
        return schoolCode;
    }

}