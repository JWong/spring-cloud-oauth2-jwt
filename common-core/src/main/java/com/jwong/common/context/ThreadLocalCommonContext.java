package com.jwong.common.context;

import java.util.HashMap;
import java.util.Map;

public class ThreadLocalCommonContext<K, V> implements CommonContext<K, V> {

    private final ThreadLocal<Map<K, V>> threadLocal = ThreadLocal.withInitial(HashMap::new);

    @Override
    public void put(K key, V value) {
        threadLocal.get().put(key, value);
    }

    @Override
    public V get(K key) {
        return threadLocal.get().get(key);
    }

    @Override
    public V remove(K key) {
        return threadLocal.get().remove(key);
    }

    @Override
    public Map<K, V> entries() {
        return threadLocal.get();
    }

}
