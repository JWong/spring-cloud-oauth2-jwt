package com.jwong.common.context;

import java.util.Map;

public interface CommonContext<K, V> {

    /**
     * put value
     * @param key
     * @param value
     */
    public void put(K key, V value);

    /**
     * get value
     * @param key
     * @return value
     */
    public V get(K key);

    /**
     * remove key and value
     * @param key
     */
    public V remove(K key);

    /**
     * @return Map<String, T>
     */
    Map<K, V> entries();

}
