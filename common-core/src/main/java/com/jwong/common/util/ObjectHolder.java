package com.jwong.common.util;

public class ObjectHolder<T> {

    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

}
