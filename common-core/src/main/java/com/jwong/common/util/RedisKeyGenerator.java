package com.jwong.common.util;

public final class RedisKeyGenerator {

    public final static String KEY_SEPARATOR = "::";

    public final static String SMS_CODE_LOGIN_PREFIX = "SMS-LOGIN";

    public final static String SMS_CODE_LOGIN_CACHE_NAME = SMS_CODE_LOGIN_PREFIX + KEY_SEPARATOR;

    public final static String SMS_CODE_KEY_SUFFIX = "SMS-CODE";

    public final static String SMS_CODE_FAIL_TIMES_SUFFIX = "FAIL_TIMES";

    public static final String TEACHER_IS_ALERT_ADD_COMMENT_SUFFIX = "TEACHER_IS_ALERT_ADD_COMMENT";

    public static final String TEACHER_IS_ALERT_ADD_COMMENT = TEACHER_IS_ALERT_ADD_COMMENT_SUFFIX + KEY_SEPARATOR;

    private RedisKeyGenerator() {}

    public static String smsCodeKey(String mobile) {
        return SMS_CODE_LOGIN_CACHE_NAME + mobile + KEY_SEPARATOR + SMS_CODE_KEY_SUFFIX;
    }

    public static String smsCodeFailTimesKey(String mobile) {
        return SMS_CODE_LOGIN_CACHE_NAME + mobile + KEY_SEPARATOR + SMS_CODE_FAIL_TIMES_SUFFIX;
    }

    public static String isAlertAddCommentKey(long teacherId, String schoolCode) {
        return TEACHER_IS_ALERT_ADD_COMMENT + schoolCode + KEY_SEPARATOR + teacherId;
    }

}
