package com.jwong.common.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Base64;

@Slf4j
public final class FileTool {

    private FileTool(){}

    public static String encryptToBase64(String filePath) {
        if (filePath == null) {
            return null;
        }
        try {
            byte[] b = Files.readAllBytes(Paths.get(filePath));
            return Base64.getEncoder().encodeToString(b);
        } catch (IOException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public static File decryptByBase64(String base64, String filePath) throws IOException {
       return Files.write(Paths.get(filePath), Base64.getDecoder().decode(base64), StandardOpenOption.CREATE).toFile();
    }

}
