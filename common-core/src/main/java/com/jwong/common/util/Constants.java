package com.jwong.common.util;

public interface Constants {

    String APP_TEACHER_CLIENT = "app-teacher";
    String APP_PARENT_CLIENT = "app-parent";
    String USER_PASS_NO_PROVIDE = "PASS_NO_PROVIDE";

    String DEFAULT_FAMILY_CARD_SCHOOL_CODE = "1101009001";

}
