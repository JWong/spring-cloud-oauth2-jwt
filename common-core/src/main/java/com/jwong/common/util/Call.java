package com.jwong.common.util;

@FunctionalInterface
public interface Call<R, T> {

    R call(T t);

}
