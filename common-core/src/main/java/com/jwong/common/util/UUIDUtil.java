package com.jwong.common.util;

import java.util.UUID;

public class UUIDUtil {

    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String genFixLenthSN(int lenth, char fillChar, String factor) {
        String result = "";
        if (factor.length() < lenth) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < lenth - factor.length(); i++) {
                sb.append(fillChar);
            }
            sb.append(factor);
            result = sb.toString();
        } else {
            result = factor.substring(0, lenth);
        }

        return result;
    }
}
 