package com.jwong.common.exception;

public class ServiceException extends BaseException {

    public ServiceException() {}

    public ServiceException(BaseErrorCode baseErrorCode) {
        super(baseErrorCode);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, BaseErrorCode baseErrorCode) {
        super(message, baseErrorCode);
    }

    public ServiceException(Throwable cause, String message, BaseErrorCode baseErrorCode) {
        super(cause, message, baseErrorCode);
    }

    public ServiceException(Throwable throwable) {
        super(throwable);
    }

    public ServiceException(Throwable throwable, String message) {
        super(throwable, message);
    }

}