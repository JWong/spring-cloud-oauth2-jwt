package com.jwong.common.exception;

public enum BaseErrorCode {

    /*
     *  系统特殊异常定义
     */
    UNAUTHORIZED("401", "不允许访问，请登录后重试", "不允许访问，请登录后重试"),
    SC_FORBIDDEN("403", "不允许访问，没有权限", "不允许访问，没有权限"),

    /**
     * Undefined error
     */
    UnknownAppError("-1", "系统异常，请稍后重试", "系统异常，请稍后重试"),

    /**
     *  100000~~10999 定义系统异常 整数(e.g. 100000,200000)
     */
    ARGUMENT_NOT_VALID("100001", "请求参数校验不通过", "请求参数校验不通过"),

    UPLOAD_FILE_SIZE_LIMIT("100002", "上传文件大小超过限制", "上传文件大小超过限制"),

    MOBILE_NUMBER_CAN_NOT_BE_NULL("000002", "手机号不能为空", "手机号不能为空,请输入手机号后重试"),
    SMS_CODE_CAN_NOT_BE_NULL("000003", "验证码不能为空", "验证码不能为空"),
    SMS_CODE_FAIL_TIMES_THRESHOLD_TRIGGER("000004", "验证码错误次数过多，请稍后重试", "验证码错误次数过多，请稍后重试"),
    SMS_CODE_IS_NOT_EXISTS("000005", "验证码不存在", "验证码不存在，请获取验证码后重试"),
    SMS_CODE_INPUT_ERROR("000006", "输入验证码错误", "输入验证码错误，请确认后重试"),
    SMS_CODE_INPUT_IS_EXPIRE("000007", "验证码已过期", "验证码已过期，请重新获取"),
    SMS_CODE_GETTING_ERROR("000008", "获取验证码异常", "获取验证码异常"),
    SMS_CODE_IS_NOT_EXPIRE("000009", "验证码尚未过期", "验证码尚未过期"),
    LOGIN_USERNAME_PASSWORD_ERROR("0000010", "用户名或密码错误", "用户名或密码错误"),
    LOGIN_ERROR("0000011", "登录异常", "登录异常"),
    ;

    /**
     * 错误码
     */
    private String errCode;

    /**
     *  错误信息.
     */
    private String errMessage;

    /**
     *  错误描述，或提示应该进行的操作以避免该类型错误
     */
    private String errDispose;

    BaseErrorCode(String errCode, String errMessage, String errDispose) {
        this.errCode = errCode;
        this.errMessage = errMessage;
        this.errDispose = errDispose;
    }

    public String getErrCode() {
        return errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public String getErrDispose() {
        return errDispose;
    }

    @Override
    public String toString() {
        return String.format("[%s] [%s] [%s]", errCode, errMessage, errDispose);
    }

}
