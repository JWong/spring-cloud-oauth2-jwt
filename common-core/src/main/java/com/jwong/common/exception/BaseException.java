package com.jwong.common.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BaseException extends RuntimeException {

    private final BaseErrorCode errorCode;

    public BaseException() {
        this(BaseErrorCode.UnknownAppError);
    }

    public BaseException(Throwable cause, String msg, BaseErrorCode errorCode) {
        super(msg, cause);
        this.errorCode = errorCode;
    }

    public BaseException(String msg) {
        this(msg, BaseErrorCode.UnknownAppError);
    }

    public BaseException(BaseErrorCode err) {
        this(err.getErrMessage(), err);
    }

    public BaseException(String msg, BaseErrorCode errorCode) {
        this(null, msg, errorCode);
    }

    public BaseException(Throwable th) {
        this(th, th.getMessage());
    }

    public BaseException(Throwable th, String msg) {
        this(th, msg, BaseErrorCode.UnknownAppError);
    }

    public BaseErrorCode getErrorCode() {
        return this.errorCode;
    }

    public static BaseException nestedException(Throwable e) {
        System.out.println();
        return nestedException("", e);
    }

    /**
     *  嵌套异常
     */
    private static BaseException nestedException(String msg, Throwable e) {
        log.error(msg, e.getMessage(), e);
        if (e instanceof BaseException) {
            return (BaseException)e;
        }
        return new BaseException(e, msg);
    }


}
