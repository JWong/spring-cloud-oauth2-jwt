package com.jwong.auth.server.rest;

import com.jwong.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class AuthorizationController {

    @Autowired
    private PersistentTokenRepository persistentTokenRepository;

    @GetMapping("/me")
    public Principal me(Principal principal) {
        return principal;
    }

    @DeleteMapping("/user/token")
    public boolean removeUserTokens(String username) {
        try {
            persistentTokenRepository.removeUserTokens(username);
            return true;
        } catch (ServiceException e) {
            return false;
        }
    }

}
