package com.jwong.auth.server.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class PermissionDO implements Serializable {

    private long permissionId;

    private long roleId;

    private String permission;

    @Override
    public boolean equals(Object other) {
        if (other instanceof PermissionDO) {
            if (permission != null) {
                return permission.equals(((PermissionDO) other).permission);
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (permission != null) {
            return permission.hashCode();
        }
        return 0;
    }

}
