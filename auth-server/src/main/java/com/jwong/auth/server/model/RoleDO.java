package com.jwong.auth.server.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class RoleDO implements Serializable {

    private long userId;

    private long roleId;

    private String roleName;

    private Set<PermissionDO> permissions;

    @Override
    public boolean equals(Object other) {
        if (other instanceof RoleDO) {
            return roleName.equals(((RoleDO) other).roleName);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return roleName.hashCode();
    }

}
