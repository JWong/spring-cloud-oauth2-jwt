package com.jwong.auth.server.mapper;

import com.jwong.auth.server.model.UserDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserAuthorityMapper {

    /**
     *  获取用户角色、资源权限
     */
    UserDO selectUserAuthority(String username);

}
