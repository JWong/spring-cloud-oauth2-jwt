package com.jwong.auth.server.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDO implements Serializable {

    private long userId;

    private String username;

    private String password;

    private String mobile;

    private String schoolCode;

    private Set<RoleDO> roles;

}
