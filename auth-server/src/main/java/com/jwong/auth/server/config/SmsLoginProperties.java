package com.jwong.auth.server.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(
        prefix = SmsLoginProperties.SMS_PROPERTIES_PREFIX,
        ignoreUnknownFields = true
)
public class SmsLoginProperties {

    public final static String SMS_PROPERTIES_PREFIX = "sms";

    /* 指定手机验证码登录的路径 */
    private String path = "/sms-login";

    /* 指定输入验证码失败的次数，超过该次数，在一定时间内，该用户将会锁定 */
    private int failTimes = 5;

    /* 是否开启用户锁定 */
    private boolean enableFail;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getFailTimes() {
        return failTimes;
    }

    public void setFailTimes(int failTimes) {
        this.failTimes = failTimes;
    }

    public boolean isEnableFail() {
        return enableFail;
    }

    public void setEnableFail(boolean enableFail) {
        this.enableFail = enableFail;
    }
}
