package com.jwong.client.service;

import com.jwong.common.entity.SmsCode;
import com.jwong.common.entity.SmsResponse;
import com.jwong.common.entity.vo.Result;

public interface SmsService {

    Result<SmsResponse> sendVoice(SmsCode smsCode);

    Result<SmsResponse> sendSingle(SmsCode smsCode);

    Result<Void> conformSmsCode(String smsCode, String mobile);

}
