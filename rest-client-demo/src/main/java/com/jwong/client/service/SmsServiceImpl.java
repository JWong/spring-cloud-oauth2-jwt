package com.jwong.client.service;

import com.jwong.common.entity.SmsCode;
import com.jwong.common.entity.SmsResponse;
import com.jwong.common.entity.vo.Result;
import org.springframework.stereotype.Service;

@Service
public class SmsServiceImpl implements SmsService {


    // 模拟发送，返回一个成功的SmsResponse
    @Override
    public Result<SmsResponse> sendVoice(SmsCode smsCode) {
        return Result.success(new SmsResponse(true, 200, "success", smsCode.getCode()));
    }

    // 模拟发送，返回一个成功的SmsResponse
    @Override
    public Result<SmsResponse> sendSingle(SmsCode smsCode) {
        return Result.success(new SmsResponse(true, 200, "success", smsCode.getCode()));
    }

    @Override
    public Result<Void> conformSmsCode(String smsCode, String mobile) {
        return Result.success();
    }
}
