package com.jwong.client.service;

import com.jwong.client.mapper.UserMapper;
import com.jwong.client.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public SysUser findUserByMobileOrUsername(String principal) {
        return userMapper.findUserByMobileOrUsername(principal);
    }

    @Override
    public SysUser findByUserId(long userId) {
        return userMapper.findByUserId(userId);
    }

}
