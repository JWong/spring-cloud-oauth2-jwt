package com.jwong.client.service;

import com.jwong.client.model.SysUser;

public interface UserService {

    SysUser findUserByMobileOrUsername(String principal);

    SysUser findByUserId(long userId);
}
