package com.jwong.client.feign;

import com.jwong.client.feign.fallback.AuthorizationFeignClientFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(
        name = "auth-server",
        fallbackFactory = AuthorizationFeignClientFallbackFactory.class
)
public interface AuthorizationFeignClient {

    @PostMapping("/oauth/token")
    ResponseEntity<Object> tokenPasswordUsername(@RequestParam("username") String username,
                                                 @RequestParam("password") String password,
                                                 @RequestParam("grant_type") String grant_type,
                                                 @RequestHeader("Authorization") String authorization)
            throws Exception;

    @PostMapping("/oauth/token")
    ResponseEntity<Object> tokenSmsCode(@RequestParam("mobile") String mobile,
                                        @RequestParam("smsCode") String smsCode,
                                        @RequestParam("grant_type") String grant_type,
                                        @RequestHeader("Authorization") String authorization)
            throws Exception;

}
