package com.jwong.client.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SysUser implements Serializable {

    private long userId;

    private String userName;

    private String mobile;

    private String schoolCode;

}
