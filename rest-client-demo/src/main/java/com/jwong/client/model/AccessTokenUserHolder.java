package com.jwong.client.model;

import com.jwong.common.entity.AccessToken;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Token和User信息持有者, 用于登录后返回给APP端")
public class AccessTokenUserHolder {

    private AccessToken accessToken;
    private SysUser sysUser;

    public AccessTokenUserHolder(AccessToken accessToken) {
        this.accessToken = accessToken;
    }

}