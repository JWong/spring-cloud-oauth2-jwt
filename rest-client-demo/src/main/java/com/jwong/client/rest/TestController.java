package com.jwong.client.rest;

import com.jwong.client.model.SysUser;
import com.jwong.client.service.UserService;
import com.jwong.common.context.PrimaryKeyContext;
import com.jwong.common.entity.vo.Result;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Token 測試")
@Slf4j
@RestController
public class TestController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/current")
    public Result<SysUser> getCurrentUser() {
        long userId = PrimaryKeyContext.get();
        return Result.success(userService.findByUserId(userId));
    }

    @GetMapping("/hello")
    public Result<String> hello() {
        return Result.success("hello");
    }


}
