package com.jwong.client.mapper;

import com.jwong.client.model.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {

    SysUser findUserByMobileOrUsername(@Param("principal") String principal);

    SysUser findByUserId(@Param("userId") long userId);
}
